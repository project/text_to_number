<?php

namespace Drupal\text_to_number\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'text_to_number' widget
 *
 * @FieldWidget(
 *   id = "text_to_number_text",
 *   module = "text_to_number",
 *   label = @Translation("Text to Number"),
 *   field_types = {
 *     "integer",
 *   }
 * )
 */
class TextNumberWidget extends WidgetBase
{

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#placeholder' => ($value == NULL) ? $this->t('Missing') : '',
      '#size' => 20,
      '#maxlength' => 30,
      '#description_display' => 'before',
      '#element_validate' => [
        [static::class, 'validate'],
      ],
    ];

    return ['value' => $element];
  }

  /**
   * Validate the number text field.
   */
  public static function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }
    if ($value == 'Missing' || $value == 'missing') {
      $sanitized_value = NULL;
    }
    else {
      $sanitized_value = preg_replace("/[^0-9]/", "", $value);
    }
    $form_state->setValueForElement($element, $sanitized_value);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'size' => 60,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];

    return $element;
  }

}
