# Text To Number

The text to number module provide a widget to number field then it applies text input into the number field and the
validation hook that will run before save. This will enable people to add text like "No value" or "Missing"
but have that turned to NULL rather than zero.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/text_to_number).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/text_to_number).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will provide a new widget to number field.

## Maintainers

- Daniel Cothran - [andileco](https://www.drupal.org/u/andileco)
- Mamadou Diao Diallo - [diaodiallo](https://www.drupal.org/u/diaodiallo)

